[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# iti-challenge

Repositório voltado para resolução do desafio do Iti.

### Descrição e Comentários

Para desenvolvimento da aplicação foi utilizada a linguagem de programação Java, em conjunto com o framework Spring.
Uma das minhas primeiras preocupações, na hora de desenvolver a solução, foi pensar uma maneira que ficasse fácil alterar 
e/ou incrementar as regras que tornam uma senha válida. Para atingir esse objetivo, acabei criando a classe `PasswordCriteria`,
onde a ideia seria manter as expressões regulares que contemplam uma senha válida.

Foram desenvolvidos testes unitários e de integração utilizando as ferramentas disponíveis na stack utilizada. 
As classes de testes são: `PasswordValidationControllerTest` e `PasswordValidationServiceTest`.

### Execução

Foram criadas tasks utilizando um Makefile para facilitar a execução do projeto.

- **Clean task**: Remove o diretório `target`.

```makefile
make clean
```

- **Compile task**: Gera o arquivo `.jar`.

```makefile
make build
```

- **Execute task**: Sobe a aplicação rodando na porta local **8080**.

```makefile
make run
```

### Documentação

A documentação no padrão OpenAPI pode ser encontrada na seguinte URL:

```
http://localhost:8080/iti/swagger-ui.html
```

O arquivo de cobertura de testes pode ser encontrado na `target/site/jacoco/index.html`, após a execução do comando:

````shell script
mvn clean verify jacoco:report
````

### Pré requisitos

- Maven 3.6.3+
- Java JDK 11
- GNU Make

# Desafio
## Descrição

Considere uma senha sendo válida quando a mesma possuir as seguintes definições:

- Nove ou mais caracteres
- Ao menos 1 dígito
- Ao menos 1 letra minúscula
- Ao menos 1 letra maiúscula
- Ao menos 1 caractere especial
    - Considere como especial os seguintes caracteres: !@#$%^&*()-+
- Não possuir caracteres repetidos dentro do conjunto

Exemplo:

```c#
IsValid("") // false  
IsValid("aa") // false  
IsValid("ab") // false  
IsValid("AAAbbbCc") // false  
IsValid("AbTp9!foo") // false  
IsValid("AbTp9!foA") // false
IsValid("AbTp9 fok") // false
IsValid("AbTp9!fok") // true
```

> **_Nota:_**  Espaços em branco não devem ser considerados como caracteres válidos.

### Problema

Construa uma aplicação que exponha uma api web que valide se uma senha é válida.

Input: Uma senha (string).  
Output: Um boolean indicando se a senha é válida.

Embora nossas aplicações sejam escritas em Kotlin e C# (.net core), você não precisa escrever sua solução usando elas. Use a linguagem de programação que considera ter mais conhecimento.