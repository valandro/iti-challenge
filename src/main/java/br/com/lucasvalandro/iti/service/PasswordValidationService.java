package br.com.lucasvalandro.iti.service;

import br.com.lucasvalandro.iti.controller.v1.request.PasswordValidationRequest;
import br.com.lucasvalandro.iti.controller.v1.response.PasswordValidationResponse;
import br.com.lucasvalandro.iti.domain.PasswordCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Slf4j
@Service
public class PasswordValidationService {

    public PasswordValidationResponse validatePassword(final PasswordValidationRequest request) {
        log.debug("Starting to validate a new password {}", request.getPassword());

        final boolean valid = Arrays.stream(PasswordCriteria.values())
                                    .allMatch(criteria -> criteria.getPredicate().test(request.getPassword()));

        return PasswordValidationResponse.builder()
                .valid(valid)
                .build();
    }
}
