package br.com.lucasvalandro.iti.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.function.Predicate;

@Getter
@AllArgsConstructor
public enum PasswordCriteria {

    NOT_EMPTY(StringUtils::isNoneEmpty),
    NOT_BLANK_SPACE(password -> password.matches(".*[^\\s-].*")),
    AT_LEAST_ONE_DIGIT(password -> password.matches(".*[0-9]+.*")),
    AT_LEAST_ONE_LOWER_CASE(password -> password.matches(".*[a-z]+.*")),
    AT_LEAST_ONE_CAPITAL_LETTER(password -> password.matches(".*[A-Z]+.*")),
    AT_LEAST_ONE_SPECIAL_LETTER(password -> password.matches(".*((!)|(@)|(#)|(\\$)|(%)|(\\^)|(&)|(\\*)|(\\))|(\\))|(-)|(\\+)).*")),
    NOT_REPEATED_LETTER(password -> password.matches("^(?:([A-Za-z]|[0-9]|((!)|(@)|(#)|(\\$)|(%)|(\\^)|(&)|(\\*)|\\)|\\)|(-)|(\\+)))(?!.*\\1))*"));

    private final Predicate<String> predicate;
}
