package br.com.lucasvalandro.iti.controller.v1;

import br.com.lucasvalandro.iti.controller.v1.request.PasswordValidationRequest;
import br.com.lucasvalandro.iti.controller.v1.response.PasswordValidationResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "iti-resource")
public interface PasswordValidationApiDefinition {

    @ApiOperation(value = "Recurso para validação de senha.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Senha validada com sucesso."),
    })
    public PasswordValidationResponse validatePassword(
            PasswordValidationRequest request
    );
}
