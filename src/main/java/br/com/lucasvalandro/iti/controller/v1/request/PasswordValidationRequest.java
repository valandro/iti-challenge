package br.com.lucasvalandro.iti.controller.v1.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PasswordValidationRequest {

    @ApiModelProperty(value = "Senha que será validada pela API.", required = true)
    private String password;
}
