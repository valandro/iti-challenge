package br.com.lucasvalandro.iti.controller.v1;

import br.com.lucasvalandro.iti.controller.v1.request.PasswordValidationRequest;
import br.com.lucasvalandro.iti.controller.v1.response.PasswordValidationResponse;
import br.com.lucasvalandro.iti.service.PasswordValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class PasswordValidationController implements PasswordValidationApiDefinition {

    @Autowired
    private PasswordValidationService service;

    @Override
    @PostMapping(value = "/password",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public PasswordValidationResponse validatePassword(@RequestBody final PasswordValidationRequest request) {
        return service.validatePassword(request);
    }
}