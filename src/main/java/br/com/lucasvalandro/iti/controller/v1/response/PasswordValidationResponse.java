package br.com.lucasvalandro.iti.controller.v1.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PasswordValidationResponse {

    @ApiModelProperty(value = "Resultado da validação da senha.", required = true)
    private boolean valid;
}
