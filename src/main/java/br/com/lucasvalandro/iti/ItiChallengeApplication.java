package br.com.lucasvalandro.iti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItiChallengeApplication {
    public static void main(String[] args) {
        SpringApplication.run(ItiChallengeApplication.class, args);
    }
}
