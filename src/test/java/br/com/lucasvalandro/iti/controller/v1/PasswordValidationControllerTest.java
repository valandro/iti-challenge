package br.com.lucasvalandro.iti.controller.v1;

import br.com.lucasvalandro.iti.controller.v1.request.PasswordValidationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class PasswordValidationControllerTest {

    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext context;

    @Before
    public void setup() {

        mockMvc = webAppContextSetup(context)
                .build();

        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
    }

    @Test
    public void shouldReturnInvalidPasswordWhenInputIsEmpty() throws Exception {
        final String url = "/v1/password";
        final PasswordValidationRequest request = PasswordValidationRequest.builder()

                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(false));
    }

    @Test
    public void shouldReturnInvalidPasswordWhenInputHasBlankSpace() throws Exception {
        final String url = "/v1/password";
        final PasswordValidationRequest request = PasswordValidationRequest.builder()
                .password(randomAlphabetic(3) + " ")
                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(false));
    }

    @Test
    public void shouldReturnInvalidPasswordWhenInputHasNoDigit() throws Exception {
        final String url = "/v1/password";
        final PasswordValidationRequest request = PasswordValidationRequest.builder()
                .password(randomAlphabetic(3))
                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(false));
    }

    @Test
    public void shouldReturnInvalidPasswordWhenInputHasNoLowerCase() throws Exception {
        final String url = "/v1/password";
        final PasswordValidationRequest request = PasswordValidationRequest.builder()
                .password(randomNumeric(2) + randomAlphabetic(3).toUpperCase())
                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(false));
    }

    @Test
    public void shouldReturnInvalidPasswordWhenInputHasNoCapitalLetter() throws Exception {
        final String url = "/v1/password";
        final PasswordValidationRequest request = PasswordValidationRequest.builder()
                .password(randomNumeric(2) + randomAlphabetic(3).toLowerCase())
                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(false));
    }

    @Test
    public void shouldReturnInvalidPasswordWhenInputHasNoSpecialLetter() throws Exception {
        final String url = "/v1/password";
        final PasswordValidationRequest request = PasswordValidationRequest.builder()
                .password(randomNumeric(2) + randomAlphabetic(3).toLowerCase() +
                        randomAlphabetic(2).toUpperCase())
                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(false));
    }

    @Test
    public void shouldReturnInvalidPasswordWhenInputHasRepeatedLetters() throws Exception {
        final String url = "/v1/password";
        final String specialLetters = "!@#$%^&*()-+";
        final int randomIndex = (int) (Math.random() * specialLetters.length());
        final String specialRandomLetter = specialLetters.substring(randomIndex - 1, randomIndex);
        final String randomLetter = randomAlphabetic(1);
        final PasswordValidationRequest request = PasswordValidationRequest.builder()
                .password(randomNumeric(1) + randomLetter.toLowerCase() +
                        randomLetter.toUpperCase() + specialRandomLetter + randomLetter.toLowerCase())
                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(false));
    }

    @Test
    public void shouldReturnValidPassword() throws Exception {
        final String url = "/v1/password";
        final String specialLetters = "!@#$%^&*()-+";
        final int randomIndex = (int) (Math.random() * specialLetters.length());
        final String specialRandomLetter = specialLetters.substring(randomIndex - 1, randomIndex);
        final String randomLetter = randomAlphabetic(1);
        final PasswordValidationRequest request = PasswordValidationRequest.builder()
                .password(randomNumeric(1) + randomLetter.toLowerCase() +
                        randomLetter.toUpperCase() + specialRandomLetter)
                .build();
        final MockHttpServletRequestBuilder postBuilder = MockMvcRequestBuilders.post(url);

        mockMvc.perform(postBuilder.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.valid").value(true));
    }
}