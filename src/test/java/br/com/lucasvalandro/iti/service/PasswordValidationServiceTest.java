package br.com.lucasvalandro.iti.service;

import br.com.lucasvalandro.iti.controller.v1.request.PasswordValidationRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class PasswordValidationServiceTest {

    @InjectMocks
    private PasswordValidationService service;

    @Test
    public void shouldReturnInvalidPassword() {
        assertFalse(service.validatePassword(mockRequest("")).isValid());
        assertFalse(service.validatePassword(mockRequest("aa")).isValid());
        assertFalse(service.validatePassword(mockRequest("ab")).isValid());
        assertFalse(service.validatePassword(mockRequest("AAAbbbCc")).isValid());
        assertFalse(service.validatePassword(mockRequest("AbTp9!foo")).isValid());
        assertFalse(service.validatePassword(mockRequest("AbTp9!foA")).isValid());
        assertFalse(service.validatePassword(mockRequest("AbTp9 fok")).isValid());
    }

    @Test
    public void shouldReturnValidPassword() {
        assertTrue(service.validatePassword(mockRequest("AbTp9!fok")).isValid());
    }

    private PasswordValidationRequest mockRequest(final String password) {
        return PasswordValidationRequest.builder().password(password).build();
    }
}
