package br.com.lucasvalandro.iti;

import org.junit.Test;

public class ItiChallengeApplicationTest {

    @Test
    public void assertConfig() {
        new ItiChallengeApplication().main(new String[] { "--spring.profiles.active=test" });
    }
}
